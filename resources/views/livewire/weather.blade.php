<div>
    <div class="h-screen flex justify-center items-center">
        @foreach ($weather as $item)
            <div class="m-auto flex place-content-center w-2/12 border-4 {{ $updateWeatherId === $item->id ? 'text-red-500' : 'text-green-500' }}">
                {{ $item->city }} | {{ $item->temperature }}
            </div>
        @endforeach
    </div>
</div>
