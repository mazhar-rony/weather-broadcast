<?php

namespace App\Console\Commands;

use App\Events\WeatherUpdated;
use App\Models\Weather;
use Illuminate\Console\Command;

class UpdateWeather extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:update-weather';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $weatherId = rand(1, 10);

        Weather::where('id', $weatherId)
            ->update([
                'temperature' => rand(1, 100)
            ]);

            $weather = Weather::find($weatherId);

            event(new WeatherUpdated($weather));
    }
}
