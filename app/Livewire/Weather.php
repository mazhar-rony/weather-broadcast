<?php

namespace App\Livewire;

use Livewire\Component;
use Livewire\Attributes\On;

class Weather extends Component
{
    public $weather;
    public $updateWeatherId = null;

    #[On('echo:update-weather,WeatherUpdated')] 
    public function updateWeather($event)
    {
        $updateWeather = $event['weather'];
        
        $this->updateWeatherId = $updateWeather['id'];

        foreach($this->weather as $item) {
            if($item->id === $updateWeather['id']) {
                $item->temperature = $updateWeather['temperature'];
            }
        }
    }

    public function render()
    {
        $this->weather = \App\Models\Weather::all();

        return view('livewire.weather')
            ->with(['weather' => $this->weather]);
    }
}
